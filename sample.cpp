#ifndef __PROGTEST__
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <memory>
#include <vector>
#include <fstream>
#include <cassert>
#include <cstring>

#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/pem.h>
using namespace std;
#endif /* __PROGTEST__ */

#define INBUFF_CAP 1024
#define OUTBUFF_CAP ( INBUFF_CAP + EVP_MAX_BLOCK_LENGTH )

class FileEncryptor {
private:
    EVP_CIPHER_CTX * ctx;
    const EVP_CIPHER * cipher;
    const char * m_PemFile;
    const char * m_Cipher;
    unsigned char iv[EVP_MAX_IV_LENGTH]{};
    unsigned char * encKey;
    int encKeyLen;
    EVP_PKEY * pKey;
public:
    FileEncryptor() : ctx(nullptr), cipher(nullptr), m_PemFile(nullptr), m_Cipher(nullptr),
                      encKey(nullptr),  encKeyLen(0), pKey(nullptr) {
        OpenSSL_add_all_ciphers();
    }
    ~FileEncryptor() {
        if (pKey) EVP_PKEY_free(pKey);
        if (ctx) EVP_CIPHER_CTX_free(ctx);
        if (encKey) free(encKey);
    }

    bool updateFile(ifstream & inFile, ofstream & outFile);
    bool init(bool seal);
    bool writeHeader(ofstream & outFile);
    bool readConfig(ifstream & inFile, const char *privateKeyFile);
    void setPemFile(const char *pemFile) { m_PemFile = pemFile; }
    void setCipher(const char *ci) { m_Cipher = ci; }
};

bool FileEncryptor::updateFile(ifstream & inFile, ofstream & outFile) {
    char inBuff[INBUFF_CAP] = {};
    char outBuff[OUTBUFF_CAP] = {};
    int outSize = 0;
    while (inFile.good() && outFile.good()) {
        inFile.read(inBuff, INBUFF_CAP);
        if (!EVP_CipherUpdate(ctx, reinterpret_cast<unsigned char *>(outBuff), &outSize,
                              reinterpret_cast<const unsigned char *>(inBuff), inFile.gcount()))
            return false;
        outFile.write(outBuff, outSize);
    }
    if (inFile.eof()) {
        if (!EVP_CipherFinal(ctx, reinterpret_cast<unsigned char *>(outBuff), &outSize))
            return false;
        outFile.write(outBuff, outSize);
        if (!outFile.good())
            return false;
        return true;
    }
    return false;
}

bool FileEncryptor::init(bool seal) {
    if (!m_PemFile || !m_Cipher)
        return false;
    if (!(ctx = EVP_CIPHER_CTX_new()))
        return false;
    if (!(cipher = EVP_get_cipherbyname(m_Cipher)))
        return false;

    FILE * pemFile = fopen ( m_PemFile, "r" );
    if ( ! pemFile )
        return false;
    if ( seal )
        pKey = PEM_read_PUBKEY ( pemFile, nullptr, nullptr, nullptr  );
    else
        pKey = PEM_read_PrivateKey ( pemFile, nullptr, nullptr, nullptr );
    fclose ( pemFile );
    if ( ! pKey )
        return false;

    if (seal) {
        encKey = (unsigned char *)malloc(EVP_PKEY_size(pKey));
        if (!EVP_SealInit(ctx, cipher, &encKey, &encKeyLen, iv, &pKey, 1))
            return false;
    } else {
        if (!EVP_OpenInit(ctx, cipher, encKey, encKeyLen, iv, pKey))
            return false;
    }
    return true;
}

bool FileEncryptor::writeHeader(ofstream & outFile) {
    if (!outFile.good())
        return false;
    int nid = EVP_CIPHER_nid(cipher);
    outFile.write(reinterpret_cast<char *>(&nid), sizeof(int));
    outFile.write(reinterpret_cast<char *>(&encKeyLen), sizeof(int));
    outFile.write(reinterpret_cast<char *>(encKey), encKeyLen);
    outFile.write(reinterpret_cast<char *>(iv), EVP_CIPHER_iv_length(cipher));
    if (!outFile.good())
        return false;
    return true;
}

bool FileEncryptor::readConfig(ifstream & inFile, const char * privateKeyFile) {
    if (!inFile.good() || !privateKeyFile)
        return false;
    m_PemFile = privateKeyFile;

    int nid = 0;
    inFile.read(reinterpret_cast<char *>(&nid), sizeof(int));
    if (inFile.gcount() != sizeof(int) || nid <= 0)
        return false;

    inFile.read(reinterpret_cast<char *>(&encKeyLen), sizeof(int));
    if (inFile.gcount() != sizeof(int) || encKeyLen <= 0)
        return false;

    if (!(cipher = EVP_get_cipherbynid(nid)))
        return false;
    m_Cipher = EVP_CIPHER_name(cipher);

    encKey = (unsigned char *)malloc(encKeyLen);
    inFile.read(reinterpret_cast<char *>(encKey), encKeyLen);
    if (inFile.gcount() != encKeyLen)
        return false;

    int ivLen = EVP_CIPHER_iv_length(cipher);
    if (ivLen != 0) {
        inFile.read(reinterpret_cast<char *>(iv), ivLen);
        if (inFile.gcount() != ivLen)
            return false;
    }
    return true;
}

class FileEncryptionManager {
public:
    static bool seal(const char *inFile, const char *outFile, const char *publicKeyFile, const char *symmetricCipher);
    static bool open(const char *inFile, const char *outFile, const char *privateKeyFile);
};

bool FileEncryptionManager::seal(const char *inFile, const char *outFile, const char *publicKeyFile, const char *symmetricCipher) {
    if(!inFile || !outFile || !publicKeyFile || !symmetricCipher) {
        return false;
    }
    FileEncryptor encryptor;
    ifstream ifs(inFile); ofstream ofs(outFile);
    if (!ifs.good() || !ofs.good()) {
        std::remove(outFile);
        return false;
    }
    encryptor.setPemFile(publicKeyFile);
    encryptor.setCipher(symmetricCipher);
    if (!encryptor.init(true)) {
        std::remove(outFile);
        return false;
    }
    if (!encryptor.writeHeader(ofs)) {
        std::remove(outFile);
        return false;
    }
    if (!encryptor.updateFile(ifs, ofs)) {
        std::remove(outFile);
        return false;
    }
    return true;
}

bool FileEncryptionManager::open(const char *inFile, const char *outFile, const char *privateKeyFile) {
    if(!inFile || !outFile || !privateKeyFile) {
        return false;
    }
    FileEncryptor encryptor;
    ifstream ifs(inFile); ofstream ofs(outFile);
    if (!ifs.good() || !ofs.good()) {
        std::remove(outFile);
        return false;
    }
    if (!encryptor.readConfig(ifs, privateKeyFile)) {
        std::remove(outFile);
        return false;
    }
    if (!encryptor.init(false)) {
        std::remove(outFile);
        return false;
    }
    if (!encryptor.updateFile(ifs, ofs)) {
        std::remove(outFile);
        return false;
    }
    return true;
}
bool seal( string_view inFile, string_view outFile, string_view publicKeyFile, string_view symmetricCipher )
{
    return FileEncryptionManager::seal(inFile.data(), outFile.data(), publicKeyFile.data(),
                                       symmetricCipher.data());
}

bool open( string_view inFile, string_view outFile, string_view privateKeyFile ){
    return FileEncryptionManager::open(inFile.data(), outFile.data(), privateKeyFile.data());
}
#ifndef __PROGTEST__

int main ( void ) {
    assert( seal("fileToEncrypt", "sealed.bin", "PublicKey.pem", "aes-128-cbc") );
    assert( open("sealed.bin", "openedFileToEncrypt", "PrivateKey.pem") );

    assert( open("sealed_sample.bin", "opened_sample.txt", "PrivateKey.pem") );

    return 0;
}

#endif /* __PROGTEST__ */
