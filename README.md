# Progtest_03

# Hybridní šifrování pomocí symetrické a asymetrické šifry

Vaším úkolem je realizovat dvě funkce (`seal` a `open`), které šifrují/dešifrují data pomocí hybridního šifrování.

## Parametry funkce `seal`

- `bool seal(string_view inFile, string_view outFile, string_view publicKeyFile, string_view symmetricCipher)`: Funkce pro šifrování dat.
  - `inFile`: vstupní soubor obsahující binární data určená k zašifrování.
  - `outFile`: výstupní soubor, kam budou uloženy všechny potřebné údaje k dešifrování.
  - `publicKeyFile`: veřejný klíč, který bude použit k zašifrování symetrického klíče.
  - `symmetricCipher`: název symetrické šifry použité pro šifrování.
  - Návratová hodnota je `true` v případě úspěchu, `false` v opačném případě. V případě selhání musí být výstupní soubor `outFile` odstraněn.

## Parametry funkce `open`

- `bool open(string_view inFile, string_view outFile, string_view privateKeyFile)`: Funkce pro dešifrování dat.
  - `inFile`: zašifrovaný soubor ve stejném formátu jako výstupní soubor z funkce `seal`.
  - `outFile`: výstupní soubor, kam budou uložena dešifrovaná data (očekává se binární shoda s vstupním souborem do funkce `seal`).
  - `privateKeyFile`: privátní klíč určený pro dešifrování zašifrovaného klíče.
  - Návratová hodnota je `true` v případě úspěchu, `false` v opačném případě. V případě selhání musí být výstupní soubor `outFile` odstraněn.

## Struktura výstupního souboru

| Pozice v souboru  | Délka   | Struktura                | Popis                                                            |
|-------------------|---------|--------------------------|------------------------------------------------------------------|
| 0                 | 4 B     | int                      | NID - identifikátor symetrické šifry(použité pro zašifrování)    |
| 4                 | 4 B     | int                      | EKlen - délka zašifrovaného klíče                                |
| 8                 | EKlen B | pole unsigned char       | Zašifrovaný klíč pomocí RSA                                      |
| 8 + EKlen         | IVlen B | pole unsigned char       | Inicializační vektor (pokud je použit)                           |
| 8 + EKlen + IVlen | —       | pole unsigned char       | Zašifrovaná data                                                 |


## Poznámky

- Funkce `seal` vygeneruje symetrický (sdílený) klíč a inicializační vektor (IV), který bude vstupem do symetrické šifry.
- Klíč k symetrické šifře je zašifrován asymetrickou šifrou (RSA) pomocí veřejného klíče.
- Funkce `open` dešifruje data pomocí privátního klíče.
- Pro šifrování a dešifrování využijte funkce z OpenSSL.
